# Vagrant for btb-crawl

## Vagrant setup

    vagrant init precise64 http://files.vagrantup.com/precise64.box

I had to upgrade the already present `precise64` box from the old 1.0.x format with:

    vagrant box repackage precise64 virtualbox

### Recipes
Using librarian-chef from librarian gem. (installed system wide with `sudo gem install librarian-chef`).

librarian-chef manages `cookbooks` directory which is .gitignored

Make sure we have all our cookbooks

    librarian-chef install 
    # or
    librarian-chef install --clean

### Adding the aws provider...    


## Constellio by hand

    #installing java
    sudo apt-get update
    sudo apt-get install openjdk-6-jre-headless



## TODO

* librarian chef: java,nodejs
* shared directory for constellio...
* re-useable apt-repo setup