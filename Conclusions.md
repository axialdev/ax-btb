
# Mash-up de recheche - BTB

D'abbord, j'aimerais remercier toute l'équipe du bureau de la traduction pour l'opportunité de travailler sur ce projet. Ce fut une collaboration réussie.

Dans le cadre du mandat sur le mash-up de recherche, nous avons effectué des expériences
afin d'addresser les constats du sous-groupe de travail sur le prototype du mash-up de recherche pour le portail d'outils d'aide a la rédaction.

Par différents procédés, nous avons réussi a créer un indexage des documents contenu dans 4 outils
d'aide a la rédaction;

* [Clefs du français pratique](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?lang=fra#)
* [Le guide du rédacteur](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/redac/index-fra.html?lang=fra)
* [Writing Tips](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/wrtps/index-fra.html?lang=fra)
* [The Canadian Style](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/tcdnstyl/index-fra.html?lang=fra#)

L'indexage des documents a été répété avec deux plateformes de recherche, soit Constellio, et LucidWorks. Les deux sont basés sur le moteur de recherche Apache Solr/Lucene.

Les documents ont été récupérés du site de production du bureau de la traduction, et ont été modifiés pour rendre l'indexage plus efficient. Les 4 outils comprenait environ 5000 documents.

* Eliminant le contenu non-pertinant (entêtes, navigation)
* Ré-écrivant des titres plus pertinents
* Ajoutant des balises de meta-information

Dans un deuxième temps nous avons exploré l'utilisation de mots clefs spécifiques à chaque document avec une collection restreinte (104) d'articles de l'outil *Writing Tips*. Nous avons exploré l'utilisation de ces étiquette pour raffiner les résultats de recherche.

## Constats

Le résultat des experimentations indique que l'approche proposée donne des résultats très prometteurs, et addressent la plupart des points relevés dans le rapport du sous-groupe.

* Nombre de résultats et pertinence
    * Trop de résultats (certains non pertinents)
    * Résultats manquants, ou peu priorisés
* Présentation
    * Titres trop longs
    * Résumé inclut des termes de l'index
    * Autres éléments inutiles

L'amélioration du processus de parcours du site (crawl), ainsi que le filtrage du contenu de la page a plusieurs impacts positifs.

L'indexage par des moteurs externes (Google, Bing) pourrait aussi bénéficier de cette même approche.

L'utilistation des mots clefs spécifiques aux documents peut aider le moteur de recherche à trouver et prioriser le contenu, mais son utilisation comme méthode de navigation (par un facette de recherche) nous semble d'une utilité marginale, et comporte une grande complexité quant a la taxonomie exacte a utiliser.

## Recommendations

Apres avoir fait ces experiences, nous concluons que 

* L'ajout du mash-up de recherche procurerait un avantage significatif a l'exploitation des outils de redaction.

* Une modification des documents pour fin d'indexage tel que nous avons propose est essentiel à la qualité des résultats de recherche.

* S'assurer que les moteurs de recherche externes (Google, Bing,..) peuvent aussi indexer des documents efficacement procurerait un avantage substantiel pour l'achalandage des ressources d'aide à la redaction.

* L'utilisation des *Tags* devrait etre inclu dans le contenu à mesure qu'il devient disponible dans les différentes collections de documents.

* L'utilisation des *Tags* pour fin de navigation offre un avantage negligeable et comporte une complexite qui n'est pas justifiée.




