# Mashup de recherche pour le Bureau de la traduction

## Local configuration notes:

* [LucidWorks](http://0.0.0.0:8989/dashboard) admin/admin
* [LucidWorks-solr](http://0.0.0.0:8888/solr/#/)
* [Constellio](http://0.0.0.0:8080/constellio/) admin/password

## Essais avec Constellio
Essayé les versions 1.3, et 2.0M2 de [constellio](http://www.constellio.com/)
Ce qui reproduit le setup du prototype de Mark.

[Constelio Architecture Blog Article](http://www.francelabs.com/blog/constellio-1-3-architecture-part-3/)

### Constellio configuration:

    connector:
    btb-web-clefs-fr
    start-url: http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?lang=fra
    inclusion RE: ^http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp

For the local config: local-en-fr

Start URLs:

    http://rd.imetrical.com:7777/clefsfp/
    http://rd.imetrical.com:7777/redac/
    http://rd.imetrical.com:7777/tcdnstyl/
    http://rd.imetrical.com:7777/wrtps/

Inclusion regexes:

    ^http://rd.imetrical.com:7777/clefsfp/
    ^http://rd.imetrical.com:7777/redac/
    ^http://rd.imetrical.com:7777/tcdnstyl/
    ^http://rd.imetrical.com:7777/wrtps/

Index Fields & Facets

    btb_idx
    btb_tool
    btb_catlng
    btb_olang # only for local-en-fr

## LucidWorks Search

Installed. 
    cd ~/Downloads/devops/LucidWorks/LucidWorksSearch/app/bin
    ./start.sh
    ./stop.sh



## SearchBlox
http://www.searchblox.com/

https://searchblox.atlassian.net/wiki/display/SD/Installation


## Apache Nutch
Combinaison de `apache-nutch-1.6` et `apache-solr-3.6.2`.

Utilise le [tutoriel ici](http://wiki.apache.org/nutch/NutchTutorial)

    export JAVA_HOME=/Library/Java/Home

Also this helps to fix `Unable to load realm info from SCDynamicStore`:

    export HADOOP_OPTS="-Djava.security.krb5.realm= -Djava.security.krb5.kdc="


Fixing the [craw script](http://mail-archives.apache.org/mod_mbox/nutch-user/201304.mbox/%3CCAPp-OAtiiB-BxccetzUu=whKUNbvUTReKx72ZNsPUiLuCdMLoQ@mail.gmail.com%3E)

    #line 125 in bin/crawl - remove "-l"
    SEGMENT=`ls $CRAWL_PATH/segments/ | sed -e "s/ /\\n/g" | egrep 20[0-9]+ | sort -n | tail -n 1`

## Clustering
Mahout is an option...

This is [Carrot2](http://project.carrot2.org/download.html)
To use carrot2 workbench with locidworks use this as the service URL:
(where local-clefsfp is the collection name)

     http://0.0.0.0:8888/solr/local-clefsfp/select

