# btb-crawl

## Mandate
Examine 4 tools:

* [Clefs du français pratique](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?lang=fra#)
* [Le guide du rédacteur](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/redac/index-fra.html?lang=fra)
* [Writing Tips](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/wrtps/index-fra.html?lang=fra)
* [The Canadian Style](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/tcdnstyl/index-fra.html?lang=fra#)

## Execute crawl and scrub

    node index.js  # clefsfp     # 2506 docs / 184M / 1275s.
    node lib/crawl-redac.js      #  409 docs /  20M /  165s.
    node lib/crawl-wrtps.js      # 1750 docs / 101M /  688s.
    node lib/crawl-tcdnstyl.js   #  408 docs /  17M /  170s.
    # Total                        5073 docs / 321M
    # Total-fr                     2915 docs
    # Total-en                     2158 docs

## Serving static content
This serves the public/ directory, so it can be crawled by Constellio/Lucidworks.
This was done because we were trying to resolve charset issues for Lucidworks, which does not display accented characters in the title field for search results, although they seem to be fine in the document details.

    http-server -p 7777
    # or
    node serve.js

### Temporary: serving from external
Map locally to rd.imetrical.com, with `/etc/hosts` record: `192.168.3.111   rd.imetrical.com`.

Also redirected external ports:`7777, 8080, 8989` to `dirac`

## Custom Crawl
Node stuff to crawl [btb site](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?lang=fra).

Naive implementation with [jsdom](https://github.com/tmpvar/jsdom).

[MapSerial for Q Gist](https://gist.github.com/tlrobinson/1033805)

## Facets
Added Facets to all three collections, from meta tags for *LucidWorks*:

    <meta name="btb.idx"../>  attr_meta_btb_idx (string,indexed,stored,facet, include in results)
    <meta name="btb.tool"../> attr_meta_btb_tool (string,indexed,stored,facet, include in results)
    <meta name="btb.catlng"../> attr_meta_btb_catlng (string,indexed,stored,multivalued,facet, include in results)    
    <meta name="btb.olang"../> attr_meta_btb_olang (string,indexed,stored,facet, include in results)    

and *Constellio* [with this technique](https://groups.google.com/forum/#!searchin/constellio/meta$20facet/constellio/A34FtzeMWEA/IeMfMywTZq0J):

    <meta name="btb.idx"../>  btb_idx (string,indexed,labe Section, meta btb_idx)
    <meta name="btb.tool"../> btb_tool (string,indexed,stored,facet, include in results)


## TODO

* Try Constellio: alternative for [Excluding Unwanted Text from the Index](https://developers.google.com/search-appliance/documentation/68/admin_crawl/Preparing#pagepart) based on google search appliance semantics.
* add [base tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base)
* separate crawl/scrub
* refactor common code - redac/clefsfp - wrtps uses href not id[2:]
* Content-Type: text/html; charset=utf-8 - or - charset=iso-8859-1, Lucid assume iso-8859-1 even when meta says utf
* [Limited Parallell](http://book.mixu.net/ch7.html)
* ---------- _Done_ ----------
* Facet (meta): Langue (local-en-fr)
* Original url (meta) + javascript _target=_new : document.querySelectorAll('meta[name="btb.url"]')[0] .attr(content)
* Add meta - categorie linguistique
* remove bottom notice from scrubbed
* make accessible from external (rd.imetrical ?) 0.0.0.0-> ??
* Document Facet creation on meta fields, Lucid,[constellio](https://groups.google.com/forum/#!searchin/constellio/meta$20facet/constellio/A34FtzeMWEA/IeMfMywTZq0J)
* add meta-data to scrubbed (keywords,description)
* <strike>store scraped as html</strike>
* clean fetched html
* <strike>crawl fetched html</strike>
* <strike>re-use local jquery</strike>
* <strike>jsdom memory leak: window.close() ok, or try [cheerio](https://github.com/MatthewMueller/cheerio)</strike>
* use carrot2, by hitting lucidworks's solr

## Notes with client
2013-06-26 Talked with Marc Belanger, to validate general approach. We will implement a Constellio implementation similar to his as a starting poit for experimentation.

Marc will send me his constellio configuration notes, including how he added a facet like feature to distinguish writing tools, as well as producion log summaries indicationg top queries per writing tool.