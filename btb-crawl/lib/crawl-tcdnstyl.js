

// Count all of the links from the nodejs build page
var request = require("request");
var jsdom = require("jsdom");
var Q = require("Q");
var fs = require('fs');
var mkdirp = require('mkdirp');
var jQuery = [fs.readFileSync("./jquery.js").toString()];

var scrapeDir = 'orig/tcdnstyl';
var scrubDir = 'public/tcdnstyl';
mkdirp.sync(scrapeDir);
mkdirp.sync(scrubDir);

// Entries
// prefer this (because subhedings are exposed)
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chapsect12&info0=12#zz12
// to this:
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chap_catlog&info0=8#zz8
// Articles
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chapsect12&info0=12#zz12
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chapsect1&info0=1#zz1
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chapsect2&info0=2#zz2
// http://www.btb.termiumplus.gc.ca/redac-chap?lang=fra&lettr=chapsect8&info0=8.2#zz8

getAllEntries().then(function(result){
  console.log("MAP SERIAL RESULT   ", JSON.stringify(result,null,2));
  var total = 0;
  var articles = []
  for (var i = 0; i<result.length; i++) {
    total+=result[i].length;
    var chapter = result[i].chapter;
    for (var j = 0; j<result[i].links.length; j++) {
      var entry = result[i].links[j]
      articles.push({chapter:chapter,id:entry.id,text:entry.text});
    }; 
  };
  console.log('-For a total of',total,'entries');
  console.log('+For a total of',articles.length,'entries');
  getAllArticles(articles); //.then();
});

function getAllEntries(){
  var deferred = Q.defer();
  var chapters = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17];
  Q.when(mapSerial(chapters, function(element, index, array) {
    return getEntries(element);
  }), function(result) {
    deferred.resolve(result);
  });
  return deferred.promise;
}

function getAllArticles(articles){
  var deferred = Q.defer();
  Q.when(mapSerial(articles, function(element, index, array) {
    return getArticle(element.chapter,element.id,element.text);
  }), function(result) {
    deferred.resolve(result);
  });
  return deferred.promise;
}

// articles: div.colLayout div.center
function getArticle(chapter,id,text){
  var deferred = Q.defer();
  console.log('Fetching chapter:',chapter,'id',id);
  var url = 'http://www.btb.termiumplus.gc.ca/tcdnstyl-chap?lang=eng&lettr=chapsect'+chapter+'&info0='+id+'#zz'+chapter;

  var pageFilename = 'page-'+chapter+'-'+id+'.html';
  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      // console.log(body) // Print the google web page.
      fs.writeFileSync(scrapeDir+'/'+pageFilename,body);
      jsdom.env({html:body,src:jQuery,done:scrubHandler});
      //deferred.resolve(46);
    } else {
      deferred.reject(new Error(error));
    }
  })

  function scrubHandler(errors, window) {
    var $ = window.$
    // should only be 1
    $articles = $('div.colLayout div.center');
    console.log("returned ", $articles.length, " articles's");
    $articles.each(function(i) {
      // console.log($(this).html());
      // remove the bottom notice
      $(this).find('p.copyright').html('');
      var content = '<html><head>'
      +'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
      +'<meta name="description" content="The Canadian Style ..." />'
      +'<meta name="keywords" content="The Canadian Style, keyword'+chapter+'" />'
      +'<meta name="btb.tool" content="The Canadian Style" />'
      +'<meta name="btb.idx" content="tcdnstyl-'+id+'" />'
      +'<meta name="btb.olang" content="English" />'
      +'<meta name="btb.catlng" content="Grammar" />'
      +'<meta name="btb.catlng" content="Punctuation &amp; Typography" />'
      +'<meta name="btb.catlng" content="Vocabulary" />'
      +'<meta name="btb.catlng" content="Usage" />'
      +'<meta name="btb.url" content="'+url+'" />'
      +'<title>'+text+'</title>'
      +'<link rel="stylesheet" type="text/css" href="/link.css">'
      +'<script src="/jquery.js"></script>'
      +'<script src="/addlink.js"></script>'
      +'</head>'
      +'<body>'
      +$(this).html()
      +'</body></html>'
      fs.writeFileSync(scrubDir+'/'+pageFilename,content);
    });
    window.close();
    deferred.resolve(42);
  };


  return deferred.promise;
}

function getEntries(chapter){
  var deferred = Q.defer();
  var url = 'http://www.btb.termiumplus.gc.ca/tcdnstyl-chap?lang=eng&lettr=chapsect'+chapter+'&info0='+chapter+'#zz'+chapter;
  console.log('Fetching chapter:',chapter);
  function handler(errors, window) {
    var $ = window.$
    // other chapters: 'div.index>ul>li>a'
    // this chapter: 'div.index>ul>li>strong>a'
    // this chapters' subheadings: ('div.index>ul>li>ul>li>a')
    // this chapter and children
    $entries = $('div.index>ul>li>strong>a,div.index>ul>li>ul>li>a');
    console.log("returned ", $entries.length, " entries's");
    var links = [];
    $entries.each(function(i) {
      // console.log($(this).html());
      $this = $(this);
      var href = $this.attr('href');
      // get the id from info0 field, in a url that looks like:
      //     /redac-chap?lang=fra&lettr=chapsect11&info0=11.2.3#zz11
      var id = href.match(/info0=(.*)#zz/)[1]; // first matching group
      var link={text:$this.attr('title'),id:id,href:href};
      //console.log(link)
      links.push(link)
    });
    window.close();
    deferred.resolve({chapter:chapter,length:$entries.length,links:links});
  };
  jsdom.env({html:url,src:jQuery,done:handler});
  return deferred.promise;
}

function mapSerial(array, callback) {
  return array.reduce(function(acc_p, element, index, array) {
    return Q.when(acc_p, function(acc) {
      return Q.when(callback(element, index, array), function(res) {    
        return acc.concat([res]);
      });
    });
  }, []);
}

