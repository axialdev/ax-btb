

// Count all of the links from the nodejs build page
var request = require("request");
var jsdom = require("jsdom");
var Q = require("Q");
var fs = require('fs');
var mkdirp = require('mkdirp');
var jQuery = [fs.readFileSync("./jquery.js").toString()];

var scrapeDir = 'orig/wt-mash-up';
var scrubDir = 'public/wt-mash-up';
mkdirp.sync(scrapeDir);
mkdirp.sync(scrubDir);

// String.fromCharCode(65).charCodeAt(0)
letters = [];
for (var l = 'a'.charCodeAt(0); l <= 'z'.charCodeAt(0); l++){
  var letter = String.fromCharCode(l);
  letters.push(letter);
}

getAllEntries().then(function(articles){
  console.log('For a total of',articles.length,'entries');
  scrubAllArticles(articles); //.then();
});

function getAllEntries(){
  console.log('Fetching all entries');
  var deferred = Q.defer();
  fs.readdir(scrapeDir, function(err,files){
    if (err){
      deferred.reject(new Error(err));
    } else {
      deferred.resolve(files);
    }
  });

  return deferred.promise;

}

function scrubAllArticles(articles){
  var deferred = Q.defer();
  Q.when(mapSerial(articles, function(element, index, array) {
    return scrubArticle(element);
  }), function(result) {
    deferred.resolve(result);
  });
  return deferred.promise;
}

// articles: div.colLayout div.center
function scrubArticle(filename){
  var deferred = Q.defer();
  console.log('Scrubbing:',filename);

  fs.readFile(scrapeDir+'/'+filename, {encoding:"utf8"},function (err, body) {
    if (err) {
      deferred.reject(new Error(err));
    } else {
      // some documents start with "<\r\n<html>"
      var badbracket = body.substr(0,9);
      if ("<\r\n<html>"===badbracket){
        console.log('+fixed leading <');
        // console.log(badbracket);
        body = body.substring(3);
      }
      //fs.writeFileSync(scrubDir+'/'+filename,body);

      jsdom.env({html:body,src:jQuery,done:scrubHandler});
      // deferred.resolve(46);
    }

  });

  function scrubHandler(errors, window) {
    var $ = window.$

    // rename      <meta name="keywords" to orig_keywords
    // and rename  <meta name="category" to keywords

    $kw = $('meta[name="keywords"]');
    $kw.attr('name','orig_keywords');
    // console.log("keywords ", $kw.attr('content'));

    $cat = $('meta[name="category"]');
    $cat.attr('name','keywords');
    // console.log("category ", $cat.attr('content'));

    // also replace ; with , in category
    var withsemicolons = $cat.attr('content');
    var re = /;/gi;
    $cat.attr('content',withsemicolons.replace(re,','));

    fs.writeFileSync(scrubDir+'/'+filename,window.document.outerHTML);
    window.close();

    deferred.resolve(42);
  };
  return deferred.promise;
}

function mapSerial(array, callback) {
  return array.reduce(function(acc_p, element, index, array) {
    return Q.when(acc_p, function(acc) {
      return Q.when(callback(element, index, array), function(res) {    
        return acc.concat([res]);
      });
    });
  }, []);
}

