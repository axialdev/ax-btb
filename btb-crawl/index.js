

// Count all of the links from the nodejs build page
var request = require("request");
var jsdom = require("jsdom");
var Q = require("Q");
var fs = require('fs');
var mkdirp = require('mkdirp');
var jQuery = [fs.readFileSync("./jquery.js").toString()];

var scrapeDir = 'orig/clefsfp';
var scrubDir = 'public/clefsfp';
mkdirp.sync(scrapeDir);
mkdirp.sync(scrubDir);

// String.fromCharCode(65).charCodeAt(0)
letters = [];
for (var l = 'a'.charCodeAt(0); l <= 'z'.charCodeAt(0); l++){
  var letter = String.fromCharCode(l);
  letters.push(letter);
}

// getArticle('z','zz9monZabbuEwg')

getAllEntries().then(function(result){
  console.log("MAP SERIAL RESULT   ", JSON.stringify(result,null,2));
  var total = 0;
  var articles = []
  for (var i = 0; i<result.length; i++) {
    total+=result[i].length;
    var letter = result[i].letter;
    for (var j = 0; j<result[i].links.length; j++) {
      var entry = result[i].links[j]
      articles.push({letter:letter,id:entry.id,text:entry.text});
    }; 
  };
  console.log('-For a total of',total,'entries');
  console.log('+For a total of',articles.length,'entries');
  getAllArticles(articles); //.then();
});

function getAllEntries(){
  var deferred = Q.defer();
  Q.when(mapSerial(letters, function(element, index, array) {
    return getEntries(element);
  }), function(result) {
    deferred.resolve(result);
  });
  return deferred.promise;
}

function getAllArticles(articles){
  var deferred = Q.defer();
  Q.when(mapSerial(articles, function(element, index, array) {
    return getArticle(element.letter,element.id,element.text);
  }), function(result) {
    deferred.resolve(result);
  });
  return deferred.promise;
}

// articles: div.colLayout div.center
function getArticle(letter,id,text){
  var deferred = Q.defer();
  var page = id.substr(2)
  console.log('Fetching letter:',letter,'page',page,'id',id);
  var url = "http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?"
  +"lang=fra&lettr=indx_catlog_"+letter+"&page="+page+".html#"+id;
  var pageFilename = 'page-'+letter+'-'+page+'.html';

  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      // console.log(body) // Print the google web page.
      fs.writeFileSync(scrapeDir+'/'+pageFilename,body);
      jsdom.env({html:body,src:jQuery,done:scrubHandler});
      deferred.resolve(46);
    } else {
      deferred.reject(new Error(error));
    }
  })

  function scrubHandler(errors, window) {
    var $ = window.$
    // should only be 1
    $articles = $('div.colLayout div.center');
    console.log("returned ", $articles.length, " articles's");
    $articles.each(function(i) {
      // console.log($(this).html());
      // remove the bottom notice
      $(this).find('p.copyright').html('');
      var content = '<html><head>'
      +'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
      +'<meta name="description" content="L\'ouvrage Clefs du fran&ccedil;ais pratique ..." />'
      +'<meta name="keywords" content="Clefs du fran&ccedil;ais pratique, keyword'+letter+'" />'
      +'<meta name="btb.tool" content="Clefs du fran&ccedil;ais pratique" />'
      +'<meta name="btb.idx" content="clefsfp-'+letter+'" />'
      +'<meta name="btb.olang" content="Fran&ccedil;ais" />'
      +'<meta name="btb.catlng" content="Grammaire" />'
      +'<meta name="btb.catlng" content="Vocabulaire" />'
      +'<meta name="btb.catlng" content="Usage" />'
      +'<meta name="btb.url" content="'+url+'" />'
      +'<title>'+text+'</title>'
      +'<link rel="stylesheet" type="text/css" href="/link.css">'
      +'<script src="/jquery.js"></script>'
      +'<script src="/addlink.js"></script>'
      +'</head>'
      +'<body>'
      +$(this).html()
      +'</body></html>'
      fs.writeFileSync(scrubDir+'/'+pageFilename,content);
    });
    window.close();
    deferred.resolve(42);
  };
  return deferred.promise;
}

function getEntries(letter){
  var deferred = Q.defer();
  var url = "http://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?lang=fra&lettr=indx_catlog_"+letter+"&page=../srchparbyalph"
  console.log('Fetching letter:',letter);
  function handler(errors, window) {
    var $ = window.$
    $entries = $('div.index li a');
    console.log("returned ", $entries.length, " entries's");
    var links = [];
    $entries.each(function(i) {
      // console.log($(this).html());
      $this = $(this);
      var link={text:$this.text(),id:$this.attr('id'),href:$this.attr('href')};
      // console.log(link)
      links.push(link)
    });
    window.close();
    deferred.resolve({letter:letter,length:$entries.length,links:links});
  };
  jsdom.env({html:url,src:jQuery,done:handler});
  return deferred.promise;
}

function mapSerial(array, callback) {
  return array.reduce(function(acc_p, element, index, array) {
    return Q.when(acc_p, function(acc) {
      return Q.when(callback(element, index, array), function(res) {    
        return acc.concat([res]);
      });
    });
  }, []);
}

