var connect = require("connect"),
    port    = parseInt(process.env.PORT, 10) || 7777;

var app = connect()
  // .use(connect.logger('dev'))
  .use(connect.static('public'))
  .use(connect.directory('public'))
  // .use(function(req, res){
  //   res.end('hello world\n');
  // })
 .listen(port);
